$(document).ready(function() {
	slickSlider();
	testimonials();
});


function slickSlider() {
	$('.carousel').slick({
	    infinite: true,
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	adaptiveHeight: true,
	  	prevArrow: '<img class="prev" src="img/prev.png">',
		nextArrow: '<img class="next" src="img/next.png">',
  });
};

var number = 1;

function testimonials() {

	if(number + 1 > 2) {
		number = 0;
	}
	number++;

	$('.comments em').hide();
	$('.comments em').eq(number - 1).show();
}


setInterval(function() {
	testimonials();
},4000);



console.log($);